using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject pantallaGameOver;
    public GameObject pantallaInicio;
    public GameObject pantallaPuntuacion;

    private void Start(){
      Time.timeScale = 0;
    }

    public void GameOver(){

      pantallaGameOver.SetActive(true);
      Time.timeScale = 0;
    }

    public void Begin(){
      pantallaInicio.SetActive(false);
      pantallaPuntuacion.SetActive(true);
      Time.timeScale = 1;
    }

    public void Replay(){
      SceneManager.LoadScene(0);
    }
}
