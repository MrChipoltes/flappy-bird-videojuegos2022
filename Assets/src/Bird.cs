using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bird : MonoBehaviour {

    public GameManager gameManager;
    private Rigidbody2D rb;

    public float velocidadImpulso;

    public int puntaje;

    public Text textoPuntaje;

    private void Awake() {
        rb = GetComponent<Rigidbody2D>();
        puntaje = 0;
    }

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            rb.velocity = Vector2.up * velocidadImpulso;
        }
    }

    public void Puntos(){
      puntaje++;
      textoPuntaje.text = puntaje.ToString();
    }

    private void OnCollisionEnter2D(Collision2D collison){
      gameManager.GameOver();
    }
}
