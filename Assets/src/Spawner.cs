using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public float tiempoMax = 1;
    private float temportizador =0;
    public GameObject tuberia;

    public float alturaTuberia;
    // Start is called before the first frame update
    void Start()
    {
        GameObject nuevaTuberia = Instantiate(tuberia);
        nuevaTuberia.transform.position = transform.position + new Vector3(0, Random.Range(-alturaTuberia, alturaTuberia), 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(temportizador > tiempoMax){
          GameObject nuevaTuberia = Instantiate(tuberia);
          nuevaTuberia.transform.position = transform.position + new Vector3(0, Random.Range(-alturaTuberia, alturaTuberia), 0);
          Destroy(nuevaTuberia, 16);
          temportizador = 0;
        }

        temportizador += Time.deltaTime;
    }
}
