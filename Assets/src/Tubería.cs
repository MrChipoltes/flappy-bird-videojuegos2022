using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tubería : MonoBehaviour {

    public float velocidad;

    private void Start() {
        StartCoroutine(MueveIzquierda());
    }

    public IEnumerator MueveIzquierda() {
        while (true) {
            transform.position += Vector3.left * velocidad * Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
    }
}
